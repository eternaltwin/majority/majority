import { IReceivedPacket, ISendPacket } from "etwin-socket-server";
import { PlayerId } from "etwin-socket-server";
import { MajorityType } from "./MajorityType";

export namespace MajorityPacket
{
	export interface Join extends ISendPacket
	{
		playerId:PlayerId;
	}
	export interface RoomInformations extends ISendPacket
	{
		players:PlayerId[];
		lives:number;
		timeBeforeNextQuestion:number;
		roomCapacity:number;
	}
	export interface Question extends ISendPacket
	{
		question:string;
		answers:MajorityType.Answer[];
	}
	export interface PlayerAnswered extends ISendPacket
	{
		playerId:PlayerId;
	}
	export interface AllAnswers extends ISendPacket
	{
		players:MajorityType.PlayerInfos[];
	}
	export interface Winners extends ISendPacket
	{
		winners:PlayerId[];
	}
	export interface PlayerMdr extends  ISendPacket
	{
		playerId:PlayerId;
	}
	export interface Answer extends IReceivedPacket
	{
		answer:MajorityType.Answer;
	}
	export interface Mdr extends IReceivedPacket {
		mdr:boolean;
	}
};