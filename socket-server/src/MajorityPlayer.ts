import { APlayer } from "etwin-socket-server";
import { MajorityType } from "./MajorityType";

export class MajorityPlayer extends APlayer
{
	public static readonly DEFAULT_LIFE_NUMBER = 3;
	public static readonly DEFAULT_MDR_NUMBER = 5;
	private _lives:number = MajorityPlayer.DEFAULT_LIFE_NUMBER;
	private _mdr:number = MajorityPlayer.DEFAULT_MDR_NUMBER;
	public answer:MajorityType.Answer;
	public isMdr:boolean = false;
	public isInMajority:boolean = true;

	public hasWrongAnswer()
	{
		this._lives--;
	}
	public laugh()
	{
		this._mdr--;
		this.isMdr = true;
	}
	public getInfos():MajorityType.PlayerInfos
	{
		return ({
			id: this.ID,
			lives: this.lives,
			answer: this.answer,
			isInMajority: this.isInMajority
		});
	}
	
	get isAlive():boolean
	{
		return (this._lives > 0);
	}
	get lives()
	{
		return (this._lives);
	}
	get mdr() {
		return (this._mdr);
	}
}