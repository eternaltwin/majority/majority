import { ARealtimeGame } from "etwin-socket-server";
import { MajorityPacket } from "./MajorityPacket";
import { MajorityPlayer } from "./MajorityPlayer";
import { MajorityType } from "./MajorityType";
import QuestionApi from "./QuestionApi";


export class MajorityGame extends ARealtimeGame<MajorityPlayer>
{
	private static readonly ROOM_CAPACITY = 5;
	private static readonly TIME_BEFORE_NEXT_QUESTION = 30;
	private static readonly NB_POSSIBLE_WINNERS = 2;
	private readonly questionApi = new QuestionApi();
	private readonly answers = new Map<MajorityType.Answer, number>();
	private nbPlayersWhoAnswered = MajorityGame.ROOM_CAPACITY;
	private nbPlayersAlive = MajorityGame.ROOM_CAPACITY;
	private interval?:NodeJS.Timeout;
	private hasEnded = false;
	
	constructor()
	{
		super(MajorityGame.ROOM_CAPACITY);
		this.registerReceiveEvent("answer", this.onPlayerAnswer);
		this.registerReceiveEvent("mdr", this.onPlayerMdr);
	}

	private clear()
	{
		this.nbPlayersWhoAnswered = 0;
		this.answers.clear();
		this.apply((player:MajorityPlayer) => {
			player.answer = undefined;
			player.isMdr = false;
		});
	}
	private prepareRoomInfosPacket():MajorityPacket.RoomInformations
	{
		const output:MajorityPacket.RoomInformations = {
			players: [], 
			lives: MajorityPlayer.DEFAULT_LIFE_NUMBER,
			timeBeforeNextQuestion: MajorityGame.TIME_BEFORE_NEXT_QUESTION,
			roomCapacity: MajorityGame.ROOM_CAPACITY
		};

		this.apply((player:MajorityPlayer) => {
			output.players.push(player.ID);
		});
		return (output);
	}
	private onPlayerAnswer = (data:MajorityPacket.Answer, player:MajorityPlayer) =>
	{
		const answeredCounter = this.answers.get(data.answer);

		if (!player.isAlive || answeredCounter == undefined || player.answer != undefined)
			return;
		player.answer = data.answer;
		this.nbPlayersWhoAnswered++;
		this.answers.set(data.answer, answeredCounter + 1);
		this.broadcast("get_player_answered", <MajorityPacket.PlayerAnswered>{playerId: player.ID});
		if (this.nbPlayersWhoAnswered >= this.nbPlayersAlive) {
			clearTimeout(this.interval!);
			this.validateTurn();
			if (!this.hasEnded)
				this.startNewTurn();
		}
	}
	private onPlayerMdr = (data:MajorityPacket.Mdr, player:MajorityPlayer) =>
	{
		if (!data.mdr || !player.isAlive || player.answer == undefined || player.isMdr || player.mdr <= 0) {
			return;
		}
		player.laugh();
		this.broadcast("get_player_mdr", <MajorityPacket.PlayerMdr>{playerId: player.ID});
	}
	private startNewTurn()
	{
		this.questionApi.getQuestion().then((question) => {
			this.clear();
			for (let i = question.answers.length - 1; i >= 0; i--) {
				this.answers.set(question.answers[i], 0);
			}
			this.broadcast("get_question", question);
		});
		this.interval = setTimeout(() => {
			this.validateTurn();
			if (!this.hasEnded)
				this.startNewTurn();
		}, MajorityGame.TIME_BEFORE_NEXT_QUESTION * 1000);
	}
	private getMostPickedAnswers():MajorityType.Answer[]
	{
		let output:MajorityType.Answer[] = [];
		let max = 0;

		this.answers.forEach((nbAnswers:number, key:MajorityType.Answer) => {
			if (nbAnswers == max) {
				output.push(key);
			}
			if (nbAnswers > max) {
				output = [key];
				max = nbAnswers;
			}
		});
		return (output);
	}
	private validateTurn()
	{
		const mostPickedAnswers = this.getMostPickedAnswers();
		const allAnswersPacket:MajorityPacket.AllAnswers = {
			players: []
		};
		const alivePlayers = this.filter((p) => p.isAlive).apply((player) => {
			const validAnswer = mostPickedAnswers.find((ans) => player.answer === ans);
			
			player.isInMajority = validAnswer != undefined;
			if (!player.isInMajority)
				player.hasWrongAnswer();
			allAnswersPacket.players.push(player.getInfos());
		}).filter((p) => p.isAlive);

		this.nbPlayersAlive = alivePlayers.length;
		this.broadcast("get_all_answers", allAnswersPacket);
		this.hasEnded = this.nbPlayersAlive <= MajorityGame.NB_POSSIBLE_WINNERS;
		if (this.hasEnded) {
			this.stop();
		}
	}
	private getWinners():MajorityPacket.Winners
	{
		const output:MajorityPacket.Winners = {
			winners: []
		};

		this.filter((p) => p.isAlive).apply((p:MajorityPlayer) => {
			output.winners.push(p.ID);
		});
		return (output);
	}

	protected run():void
	{
		this.startNewTurn();
	}
	protected onJoin(p:MajorityPlayer)
	{
		this.broadcast("on_player_join", <MajorityPacket.Join>{playerId: p.ID});
		p.send("get_room_infos", this.prepareRoomInfosPacket());
	}
	protected close():void
	{
		clearTimeout(this.interval!);
		this.broadcast("get_winners", this.getWinners());
	}
};