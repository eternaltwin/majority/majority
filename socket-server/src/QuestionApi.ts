import fetch from "node-fetch";
import { MajorityPacket } from "./MajorityPacket";

export default class QuestionApi
{
	private static readonly URL:string = "https://niko.ovh/API/majority.php";
	
	public async getQuestion():Promise<MajorityPacket.Question>
	{
		const PARAMS = {
			method: "GET"
		};
		
		return (await fetch(QuestionApi.URL, PARAMS).then((res) => {
			return (res.json());
		}).catch((err) => {
			console.error("Promise failed: ", err);
		}));
	}
};