import { PlayerId } from "etwin-socket-server";

export module MajorityType
{
	export type Answer = string|undefined;
	export type PlayerInfos = {
		answer:MajorityType.Answer, 
		lives:number, 
		isInMajority:boolean, 
		id:PlayerId
	};
}