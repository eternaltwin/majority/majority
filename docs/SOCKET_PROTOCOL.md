# Majority's Socket Protocol

This files describe every event you can send/receive as a client.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Join event

Event name: **on_player_join**

Description:
This event is triggered when a user join your room.

Content:
* playerId:PlayerId

## Room Informations event

Event name: **get_room_infos**

Description:
This event let you get all current players in the room.
It also give to you the number of life you'll have when you start.

Content:
* players:PlayerId[]
* lives:number
* timeBeforeNextQuestion:number;
* roomCapacity:number;

## Question event

Event name: **get_question**

Description:
This event is sent to indicate the start of a new round.
It will send to you the new question and its possible answers.

Content:
* question:string
* answers:MajorityType.Answer[]

## Player Answered event

Event name: **get_player_answered**

Description:
This event is triggered when another player answered to the question.
It will send you the id of the player who answered.

Content:
* playerId:PlayerId

## Get All Answers event

Event name: **get_all_answers**

Description:
This event closes a turn, and will send you a sumary of the actual state of every player.
You will only receive the informations of players who are alive.

Content:
* playerId:MajorityType.PlayerInfos[];

## Get Winners event

Event name: **get_winners**

Description:
This event closes the game and the connection with the server.
It will send you the id of the players who won.

Content:
* winners:PlayerId[];

## Packet sent from client to server-side

## Send Answer event

Event name: **answer**

Description:
Send the user's answer to the Majority's question to the server.
*Nothing will happen in the following cases:*
* *The user has lost the game.*
* *The answer to the question isn't in the proposed choices.*
* *The user already sent a "answer" user and try to send a second during the same turn.*

Content:
* answer:MajorityType.Answer;

## Objects

### PlayerId

PlayerId is a simple string.

### MajorityType.Answer

This object is also a simple string that can be undefined.

### MajorityType.PlayerInfos

This object contains all the players infos.

Content:
* id:PlayerId
* lives:number
* answer: Majority